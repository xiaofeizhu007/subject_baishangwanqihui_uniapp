import Config from './config.js'

export default {
	/**
	 * 页面跳转
	 * @param {string} to 跳转链接 /pages/idnex/index
	 * @param {Object} param 参数 {key : value, ...}
	 * @param {string} mode 模式 
	 */
	redirectTo(to, param, mode) {
		// console.log(to)
		let url = to;
		if (param != undefined) {
			Object.keys(param).forEach(function(key) {
				if (url.indexOf('?') != -1) {
					url += "&" + key + "=" + param[key];
				} else {
					url += "?" + key + "=" + param[key];
				}
			});
		}
		switch (mode) {
			case 'tabbar':
				// 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面。
				uni.switchTab({
					url: url
				})
				break;
			case 'redirectTo':
				// 关闭当前页面，跳转到应用内的某个页面。
				uni.redirectTo({
					url: url
				});
				break;
			case 'reLaunch':
				// 关闭所有页面，打开到应用内的某个页面。
				uni.reLaunch({
					url: url
				});
				break;
			default:
				// 保留当前页面，跳转到应用内的某个页面
				uni.navigateTo({
					url: url
				});
		}
	},
	/**
	 * 图片路径转换
	 * @param {String} img_path 图片地址
	 * @param {Object} params 参数，针对商品、相册里面的图片区分大中小，size: big、mid、small
	 */
	img(img_path, params) {
		var path = "";
		if (img_path != undefined && img_path != "") {
			if (params && img_path != this.getDefaultImage().default_goods_img) {
				// 过滤默认图
				let arr = img_path.split(".");
				let suffix = arr[arr.length - 1];
				arr.pop();
				if(params.size){
					arr[arr.length - 1] = arr[arr.length - 1] + "_" + params.size;
				}
				arr.push(suffix);
				img_path = arr.join(".");
			}
			if (img_path.indexOf("http://") == -1 && img_path.indexOf("https://") == -1) {
				path = Config.imgDomain + "/" + img_path;
			} else {
				path = img_path;
			}
		}
		return path;
	},
	//商品图片类型转换
	goodsImgType(type, source){
	   source = source || 0 ;
	   //候鸟类型要做转换
	   if(parseInt(source) === 1){
	      let transfer = {
	         big : 'big',
	         mid : 'thumbnail',
	         small : 'tiny',
	      };
	      type = transfer[type] || '';
	   }
	   //唯品类型显示原图
	   if(parseInt(source) === 2 || parseInt(source) === 3){
		   type = '';
	   }
	   return type;
	},
	/**
	 * 时间戳转日期格式
	 * @param {Object} timeStamp
	 */
	timeStampTurnTime(timeStamp) {
		if (timeStamp != undefined && timeStamp != "" && timeStamp > 0) {
			var date = new Date();
			date.setTime(timeStamp * 1000);
			var y = date.getFullYear();
			var m = date.getMonth() + 1;
			m = m < 10 ? ('0' + m) : m;
			var d = date.getDate();
			d = d < 10 ? ('0' + d) : d;
			var h = date.getHours();
			h = h < 10 ? ('0' + h) : h;
			var minute = date.getMinutes();
			var second = date.getSeconds();
			minute = minute < 10 ? ('0' + minute) : minute;
			second = second < 10 ? ('0' + second) : second;
			return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
		} else {
			return "";
		}
	},
	/**
	 * 倒计时
	 * @param {Object} seconds 秒
	 */
	countDown(seconds) {
		let [day, hour, minute, second] = [0, 0, 0, 0]
		if (seconds > 0) {
			day = Math.floor(seconds / (60 * 60 * 24))
			hour = Math.floor(seconds / (60 * 60)) - (day * 24)
			minute = Math.floor(seconds / 60) - (day * 24 * 60) - (hour * 60)
			second = Math.floor(seconds) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60)
		}
		if (day < 10) {
			// day = '0' + day
		}
		if (hour < 10) {
			// hour = '0' + hour
		}
		if (minute < 10) {
			// minute = '0' + minute
		}
		if (second < 10) {
			// second = '0' + second
		}
		return {
			d: day,
			h: hour,
			i: minute,
			s: second
		};
	},
	/**
	 * 数值去重
	 * @param {Array} arr 数组
	 * @param {string} field 字段
	 */
	unique(arr, field) {
		const res = new Map();
		return arr.filter((a) => !res.has(a[field]) && res.set(a[field], 1));
	},
	/**
	 * 判断值是否在数组中
	 * @param {Object} elem
	 * @param {Object} arr
	 * @param {Object} i
	 */
	inArray: function(elem, arr) {
		return arr == null ? -1 : arr.indexOf(elem);
	},
	/**
	 * 获取某天日期
	 * @param {Object} day
	 */
	getDay: function(day) {
		var today = new Date();
		var targetday_milliseconds = today.getTime() + 1000 * 60 * 60 * 24 * day;
		today.setTime(targetday_milliseconds);

		const doHandleMonth = function(month) {
			var m = month;
			if (month.toString().length == 1) {
				m = "0" + month;
			}
			return m
		}

		var tYear = today.getFullYear();
		var tMonth = today.getMonth();
		var tDate = today.getDate();
		var tWeek = today.getDay();
		var time = parseInt(today.getTime() / 1000);
		tMonth = doHandleMonth(tMonth + 1);
		tDate = doHandleMonth(tDate);

		const week = ['周日', '周一', '周二', '周三', '周四', '周五', '周六']
		return {
			't': time,
			'y': tYear,
			'm': tMonth,
			'd': tDate,
			'w': week[tWeek]
		};
	},
	/**
	 * 图片选择加上传
	 * @param number num
	 * @param {Object} params
	 * @param {Object} callback
	 * return array
	 */
	upload: function(num, params, callback) {
		var imgs_num = num;
		uni.chooseImage({
			count: imgs_num,
			sizeType: ['compressed'], //可以指定是原图还是压缩图，默认二者都有
			sourceType: ['album', 'camera'], //从相册或者拍照
			success:async res => {
				const tempFilePaths = res.tempFilePaths;
				var imgs = [];
				for (var i = 0; i < tempFilePaths.length; i++) {
					//console.log(params,'djkfej')
					//console.log('压缩前图片体积',res.tempFiles[0].size);
								// let path_temp = res.tempFilePaths[0],imgs_temp='';
								// //压缩图片
								//  plus.zip.compressImage({
								// 		src:path_temp,
								// 		dst:path_temp,
								// 		overwrite:true,//是否生成新图片
								// 		quality:20,//1-100,1图片最小，100图片最大
								// 		width:'auto',
								// 		height:'auto'
								// 		},
								// 		async result => {
								// 			let imgPathUrl = result.target;
								// 			let imgPathSize= result.size;
								// 			console.log('压缩后的体积',imgPathSize);
								// 			var path =  await this.upload_file_server(imgPathUrl, params, params.method);
								// 			imgs_temp = path
								// 			imgs.push(path);
								// 			console.log(path,789,imgs)	
											
											
								// 		})
								// imgs.push(imgs_temp);
								// console.log(path,789,imgs)	
					var path = await this.upload_file_server(tempFilePaths[i], params, params.method);
					imgs.push(path);
				}
				console.log(imgs,888888)
				typeof callback == 'function' && callback(imgs);
			}
		});
	},
	//上传
	upload_file_server(tempFilePath, params, method) {
		var data = {
			token: uni.getStorageSync('token'),
			Access_Token: uni.getStorageSync('Access_Token'),
			private_key: Config.privateKey
		}
		data = Object.assign(data, params);
		return new Promise((resolve, reject) => {
			
					// #ifdef APP-PLUS
					let imgPathUrl = '';
					uni.uploadFile({
						url: Config.baseUrl + '/api/upload/' + method,
						filePath: tempFilePath,
						name: 'file',
						formData: data,
						success: function(res) {
							//console.log(res.data,'hhh')
							var path_str = JSON.parse(res.data);
							if (path_str.code >= 0) {
								resolve(path_str.data.pic_path || path_str.data.path);
							} else {
								reject("error");
							}
						}
					});
							
					// #endif
					console.log(params,'params')
					console.log(method,'methods')
					// #ifndef APP-PLUS
					uni.uploadFile({
						url: Config.baseUrl + '/api/upload/' + method,
						filePath: tempFilePath,
						name: 'file',
						formData: data,
						success: function(res) {
							console.log(res.data,'hhh')
							var path_str = JSON.parse(res.data);
							if (path_str.code >= 0) {
								resolve(path_str.data.pic_path || path_str.data.path);
							} else {
								reject("error");
							}
						}
					});
					// #endif
					
					
		});
	
	},
	/**
		 * 判断手机是否为苹果系列
		 */
		phoneType() {
			let isIphone = false;
			if (uni.getSystemInfoSync().platform == 'ios') {
				isIphone = true;
			} else {
				isIphone = false
			}
			return isIphone;
		},
	/**
	 * 复制
	 * @param {Object} message
	 * @param {Object} callback
	 */
	copy(value, callback) {
		// #ifdef H5
		var oInput = document.createElement('input'); //创建一个隐藏input（重要！）
		oInput.value = value; //赋值
		document.body.appendChild(oInput);
		oInput.select(); // 选择对象
		document.execCommand("Copy"); // 执行浏览器复制命令
		oInput.className = 'oInput';
		oInput.style.display = 'none';
		this.showToast({
			title: '复制成功'
		});
		typeof callback == 'function' && callback();
		// #endif

		// #ifdef MP || APP-PLUS
		uni.setClipboardData({
			data: value,
			success: () => {
				typeof callback == 'function' && callback();
			}
		});
		// #endif
	},
	/**
	 * 是否是微信浏览器
	 */
	isWeiXin() {
		// #ifndef H5
		return false;
		// #endif
		var ua = navigator.userAgent.toLowerCase();
		if (ua.match(/MicroMessenger/i) == "micromessenger") {
			return true;
		} else {
			return false;
		}
	},

	/**
	 * 显示消息提示框
	 *  @param {Object} params 参数
	 */
	showToast(params = {}) {
		params.title = params.title || "";
		params.icon = params.icon || "none";
		params.position = params.position || 'bottom';
		uni.showToast(params);
	},
	/**
	 * 检测苹果X以上的手机
	 */
	isIPhoneX() {
		let res = uni.getSystemInfoSync();
		if (res.model.search('iPhone X') != -1) {
			return true;
		}
		return false;
	},
	/**
	 * 深度拷贝对象
	 * @param {Object} obj
	 */
	deepClone(obj) {
		const isObject = function(obj) {
			return typeof obj == 'object';
		}

		if (!isObject(obj)) {
			throw new Error('obj 不是一个对象！')
		}
		//判断传进来的是对象还是数组
		let isArray = Array.isArray(obj)
		let cloneObj = isArray ? [] : {}
		//通过for...in来拷贝
		for (let key in obj) {
			cloneObj[key] = isObject(obj[key]) ? this.deepClone(obj[key]) : obj[key]
		}
		return cloneObj
	},
	refreshBottomNav() {
		var bottomNav = uni.getStorageSync("bottom_nav");
		bottomNav = JSON.parse(bottomNav);
		for (var i = 0; i < bottomNav.list.length; i++) {
			var item = bottomNav.list[i];
			var obj = {
				index: i
			};
			obj.text = item.title;
			obj.iconPath = this.img(item.iconPath);
			obj.selectedIconPath = this.img(item.selectedIconPath);
			if (bottomNav.type == 1) {
				// 图文
			} else if (bottomNav.type == 2) {
				// 图片
			} else if (bottomNav.type == 3) {
				// 文字
			}
			uni.setTabBarItem(obj);
		}
	},
	/**
	 * 自定义模板的跳转链接
	 * @param {Object} link
	 */
	diyRedirectTo(link, method) {
		if (link == null || link == '' || !link.wap_url) return;
		if (link.wap_url.indexOf('http') != -1) {
			this.redirectTo('/otherpages/web/web?src=' + link.wap_url);
		}else {
			if(link.site_id){
				this.redirectTo(link.wap_url,{site_id:link.site_id});
			}else{
				this.redirectTo(link.wap_url);
			}
		}
	},
	/**
	 * 获取默认图
	 * @param {Object} link
	 */
	getDefaultImage() {
		let defaultImg = uni.getStorageSync('default_img');
		if (defaultImg) {
			defaultImg = JSON.parse(defaultImg);
			defaultImg.default_goods_img = this.img(defaultImg.default_goods_img);
			defaultImg.default_headimg = this.img(defaultImg.default_headimg);
			defaultImg.default_shop_img = this.img(defaultImg.default_shop_img);
			return defaultImg;
		} else {
			return {
				default_goods_img: '',
				default_headimg: '',
				default_shop_img: ''
			};
		}
	},

	/**
	 * 判断手机是否为iphoneX系列
	 */
	uniappIsIPhoneX() {
		let isIphoneX = false;
		let systemInfo = uni.getSystemInfoSync();
		// #ifdef MP || APP-PLUS
		if (systemInfo.model.search('iPhone X') != -1) {
			isIphoneX = true;
		}else if (systemInfo.model.search('iPhone 11') != -1) {
			isIphoneX = true;
		}
		// #endif

		// #ifdef H5
		var u = navigator.userAgent;
		var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
		if (isIOS) {
			if (systemInfo.screenWidth == 375 && systemInfo.screenHeight == 812 && systemInfo.pixelRatio == 3) {
				isIphoneX = true;
			} else if (systemInfo.screenWidth == 414 && systemInfo.screenHeight == 896 && systemInfo.pixelRatio == 3) {
				isIphoneX = true;
			} else if (systemInfo.screenWidth == 414 && systemInfo.screenHeight == 896 && systemInfo.pixelRatio == 2) {
				isIphoneX = true;
			}
		}
		// #endif
		return isIphoneX;
	},
	/**
	 * 判断手机是否为iphoneX系列   	判断当前页面栈是否存在，如果存在，则返回该页面栈，否则跳转到该页面
	 */
	jumpPage(url) {
		let jump = true;
		let arr = getCurrentPages().reverse();
		for (let i = 0; i < arr.length; i++) {
			if (url.indexOf(arr[i].route) != -1) {
				jump = false;
				uni.navigateBack({
					delta: i
				});
				break;
			}
		}
		if (jump) {
			this.$util.diyRedirectTo(url);
		}
	},
	numberFixed(e,f){
		if(!f){
			f=0;
		}
		return Number(e).toFixed(f);
	},
	// 自定义返回事件
	goBack(){
		if(getCurrentPages().length > 1){
			uni.navigateBack()
		}else{
			uni.navigateTo({
				url: '/pages/index/index/index'
			})
		}
	},
	/**
	 * 判断是否为ios系统
	 * @param number version
	 */
	isIos(version) {
		try {
			// #ifdef APP-PLUS
			let res = uni.getSystemInfoSync();
			let v = res.system.substr(0, 2);
			if (res.platform === 'ios') {
				if (version && (res.system.substr(0, 2) < version)) {
					return false;
				}
				return true;
			}
			// #endif
		} catch (e) {}
		
		return false;
	},
}
