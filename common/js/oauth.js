export default {
	data() {
		return {
			// 运营商
			provider: [],
			// 第三方登录配置
			oauthConfig: null,
		}
	},
	onLoad(option) {
		// #ifdef APP-PLUS
		
		// 获取运营商
		this.getProvider();
		
		// 获取第三方登录配置
		this.getOauthConfig();
		// #endif
	},
	methods: {
		/**
		 * 获取运营商
		 */
		getProvider() {
			uni.getProvider({
				service: 'oauth',
				success: res => {
					this.provider = res.provider;
				},
				fail: res => {}
			});
		},
		
		/**
		 * 获取第三方登录配置
		 */
		getOauthConfig() {
			this.$api.sendRequest({
				url: '/api/login/oauthConfig',
				data: {},
				success: res => {
					if (res.code >= 0) {
						this.oauthConfig = res.data.value;
					}
				},
				fail: res => {}
			})
		},
		
		/**
		 * 点击登录
		 * @param {Object} type
		 */
		oauthLogin(type) {
			switch (type) {
				case 'weixin': 
					this.wchatOauthLogin();
					break;
				case 'qq':
					this.qqOauthLogin();
					break;
				case 'apple':
					this.appleOauthLogin();
					break;
			}
		},
		
		/**
		 * 登录
		 */
		oauthLoginSubmit(auth_tag, auth_openid) {
			if (!auth_tag || !auth_openid) {
				this.$util.showToast({
					title: '参数缺失'
				});
				return;
			}
			
			let data = {
				auth_tag: auth_tag,
				auth_openid: auth_openid
			};
			
			if (this.isSub) return;
			this.isSub = true;
			
			uni.showLoading({
			    title: '登录中',
				mask: true
			});
								
			this.$api.sendRequest({
				url: '/api/login/auth',
				data: data,
				success: res => {
					
					if (res.code >= 0) {
						uni.setStorage({
							key: 'token',
							data: res.data.token,
							success: () => {
								uni.removeStorageSync('loginLock');
								uni.removeStorageSync('unbound');
								uni.removeStorageSync('authInfo');
								if (this.back != '') {
									this.$util.redirectTo(this.back, {}, this.redirect);
								} else {
									this.$util.redirectTo('/pages/member/index/index', {}, this.redirect);
								}
							},
							fail: res => {
								this.isSub = false;
								this.$util.showToast({
									title: '登录失败'
								});
							}
						});
					} else {
						this.isSub = false;
						
						if (res.error_code === 'MEMBER_NOT_EXIST') {
							// 会员不存在, 跳转一个绑定页面, 实现手机号绑定(暂时跳转注册页面)
							uni.setStorage({
								key: 'authInfo',
								data: {
									authInfo: data,
									userInfo: this.userInfo
								}
							});
							this.$util.redirectTo('/pages/login/register/register', {}, this.redirect);
						} else {
							this.$util.showToast({
								title: res.message
							});
						}
					}
				},
				fail: res => {
					this.isSub = false;
					this.$util.showToast({
						title: '登录失败'
					});
				},
				complete: res => {
					uni.hideLoading();
				}
			});
		},
		
		/**
		 * uniapp 登录
		 * @param {Object} type
		 */
		uniLogin(type) {
			return new Promise((resolve, reject) => {
				var is_open = 0;
				
				switch (type) {
					case 'weixin': 
						is_open = this.oauthConfig ? this.oauthConfig.wechat.is_open : 0;
						break;
					case 'qq':
						is_open = this.oauthConfig ? this.oauthConfig.qq.is_open : 0;
						break;
					case 'apple':
						is_open = this.oauthConfig ? this.oauthConfig.apple.is_open : 0;
						break;
				}
				if (is_open != 1) {
					reject('该第三方登录未开启');
					return;
				}
				
				if (this.provider.indexOf(type) === -1) {
					reject('该第三方登录未配置');
					return;
				}
				
				// 登录
				uni.login({
					provider: type,
					success: res => {
						
						// 获取用户信息
						uni.getUserInfo({
							provider: type,
							success: res => {
								
								if (res.errMsg == 'getUserInfo:ok') {
									this.userInfo = res.userInfo;
									resolve(res.userInfo);
								} else {
									reject('信息获取失败');
								}
							},
							fail: res => {
								reject('信息获取失败');
							}
						});
					},
					fail: res => {
						reject('登录失败');
					}
				});
			});
		},
		
		/**
		 * QQ登录
		 */
		qqOauthLogin() {
			this.uniLogin('qq')
				.then(res => {
					this.oauthLoginSubmit('qq_openid', res.openId);
				})
				.catch(reson => {
					this.$util.showToast({
						title: reson
					});
				});
		},
		
		
		/**
		 * 微信登录
		 */
		wchatOauthLogin() {
			this.uniLogin('weixin')
				.then(res => {
					let auth_tag = '',
						auth_openid = '';
						
					if (typeof(res.unionId) !== 'undefined' && res.unionId !== '') {
						auth_tag = 'wx_unionid';
						auth_openid = res.unionId;
					} else {
						auth_tag = 'wx_openid';
						auth_openid = res.openId;
					}
					
					this.oauthLoginSubmit(auth_tag, auth_openid);
				})
				.catch(reson => {
					this.$util.showToast({
						title: reson
					});
				});
		},
		
		/**
		 * 苹果登录
		 */
		appleOauthLogin() {
			this.uniLogin('apple')
				.then(res => {
					this.oauthLoginSubmit('apple_openid', res.openId);
				})
				.catch(reson => {
					this.$util.showToast({
						title: reson
					});
				});
		},
	}
}
