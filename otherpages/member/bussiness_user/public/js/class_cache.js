export default {
	methods:{
		getTeamOrganizationAndMember(){
			let res;
			if (uni.getStorageSync('teamOrganizationAndMember')) {
				res = uni.getStorageSync('teamOrganizationAndMember');
				if (res.code == 0) {
					if (res.data.is_have_team) {
						this.data = res.data.tree;
						this.memberTotalNum = res.data.member_total_num;
						this.children = [];
						this.childrenBeifen = [];
						for (let key in this.data) {
							this.data[key].child_num = Object.keys(this.data[key].child).length
							this.children.push(this.data[key]);
							this.childrenBeifen.push(this.data[key]);
						}
				
						if (this.$refs['loadingCover']) this.$refs['loadingCover'].hide();
					} else {
						this.$util.showToast({
							title: '您还没有团队'
						});
					}
				} else {
					this.$util.showToast({
						title: res.message
					});
				}
				return;
			}
			this.$api.sendRequest({
				url: '/yunchuang/api/team/getTeamOrganizationAndMember',
				data:{
					team_id:this.team_id
				},
				success: res => {
					if (res.code >= 0) {
				
						if (res.data.is_have_team) {
							this.data = res.data.tree;
							this.memberTotalNum = res.data.member_total_num;
							uni.setStorageSync('teamOrganizationAndMember',res)
							this.children = [];
							this.childrenBeifen = [];
							for (let key in this.data) {
								this.data[key].child_num = Object.keys(this.data[key].child).length
								this.children.push(this.data[key]);
								this.childrenBeifen.push(this.data[key]);
							}
			
							if (this.$refs['loadingCover']) this.$refs['loadingCover'].hide();
						} else {
							this.$util.showToast({
								title: '您还没有团队'
							});
						}
					} else {
						this.$util.showToast({
							title: res.message
						});
						uni.navigateBack({
							
						})
					}
					if (this.$refs.loadingCover) this.$refs.loadingCover.hide();
				}
			});
		}
	}
	
}