export default{
	methods:{
		getTeamOrganization(){
			
			if(uni.getStorageSync('TeamOrganization')){
				var res = uni.getStorageSync('TeamOrganization');
				if (res.data.is_have_team) {
					this.data = res.data.tree;
					// this.memberTotalNum = res.data.member_total_num;
					this.children = [];
					this.childrenBeifen = [];
					for (let key in this.data) {
						this.data[key].child_num = Object.keys(this.data[key].child).length
						this.children.push(this.data[key]);
						this.childrenBeifen.push(this.data[key]);
					}
				
				} else {
					this.$util.showToast({ title: '您还没有团队' });
				}
				return;
			}
			this.$api.sendRequest({
					url: '/yunchuang/api/team/getTeamOrganization',
					data:{
						team_id:this.team_id
					},
					success: res => {
						if (res.code == 0) {
							uni.setStorageSync('TeamOrganization',res)
							if (res.data.is_have_team) {
								this.data = res.data.tree;
								this.children = [];
								this.childrenBeifen = [];
								for (let key in this.data) {
									this.data[key].child_num = Object.keys(this.data[key].child).length
									this.children.push(this.data[key]);
									this.childrenBeifen.push(this.data[key]);
								}
				
							} else {
								this.$util.showToast({ title: '您还没有团队' });
							}
						} else {
							this.$util.showToast({ title: res.message });
						}
						if (this.$refs.loadingCover) this.$refs.loadingCover.hide();
					}
				});
				
		},
	}
}