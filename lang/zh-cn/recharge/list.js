export const lang = {
	//title为每个页面的标题
	title: '充值列表',
	faceValue: '面值',
	point: '积分',
	growth: '成长值',
	give: '额外赠送',
	rechargeRecord: '充值记录',
	rechargeList: '充值套餐列表',
	recharge: '充值',
	accountBalance: '当前余额 (元)'
}
