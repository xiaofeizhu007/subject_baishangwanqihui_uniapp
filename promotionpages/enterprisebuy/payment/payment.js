export default {
	data() {
		return {
			network:false,
			currInvoiceIndex:'',
			orderInvoice:{
				orderInvoiceCurr:'',
				orderInvoice:{
					invoice_status:'',
					invoice_rate:'',
					invoice_content:'',
					invoice_money:''
				},
				invoice_config:{
					invoice_content:[]
				},
				invoice_content:'',
				invoice_conten_index:0,
				is_invoice:0,//是否需要发票
				invoice_type:1, //发票类型电子、纸质
				invoice_title_type:1,  // 抬头类型
				
				invoice_title:'',//发票抬头
				invoice_full_address:'', //邮寄地址
				is_tax_invoice:0, //是否需要增值税发票
				invoice_email:'', // 邮箱
				invoice_content:'', //发票内容
				taxpayer_number:'', //纳税人识别号
				
			},
			orderInvoicebak:{
				orderInvoiceCurr:'',
				orderInvoice:{
					invoice_status:'',
					invoice_rate:'',
					invoice_content:'',
					invoice_money:''
				},
				invoice_config:{
					invoice_content:[]
				},
				invoice_content:'',
				invoice_conten_index:0,
				is_invoice:0,//是否需要发票
				invoice_type:1, //发票类型电子、纸质
				invoice_title_type:1,  // 抬头类型
				invoice_title:'',//发票抬头
				invoice_full_address:'', //邮寄地址
				is_tax_invoice:0, //是否需要增值税发票
				invoice_email:'', // 邮箱
				invoice_content:'', //发票内容
				taxpayer_number:'', //纳税人识别号
			},
			orderCreateData: {
				is_balance: 0,
				id_card: '',
				real_name: '',
				pay_password: '',
				platform_coupon_id:0,
				buyer_message:{}
				
			},
			orderPaymentData: {
				member_account: {
					balance: 0,
					is_pay_password: 0
				},
				platform_coupon_list:[]
				
			},
			presentCoupon:[],//优惠券赠送
			isSub: false,
			sitePromotion: [],
			siteDelivery: {
				site_id: 0,
				data: []
			},
			siteCoupon: {
				site_id: 0,
				data: []
			},
			isFocus:false,
			tempData: null,
			
			
			selectCouponId:0,
			selectCouponMoney:'0.00',
			
			selectPlatCouponId:0,
			selectPlatCouponMoney:'0.00',
		};
	},
	onShow(){
		this.network = true
	},
	methods: {
		//打开发票弹窗
		openInvoicePopup(val){
			
			this.network = true;
			
			this.orderInvoice = this.orderInvoicebak;
			this.currInvoiceIndex = '';
			console.log(this.orderInvoice)
			this.orderInvoice = this.$util.deepClone(val);
			this.orderInvoice.orderInvoiceCurr = val.site_id;
			this.orderInvoice.invoice_type = this.orderInvoice.invoice_type == undefined ? 1:this.orderInvoice.invoice_type;
			this.orderInvoice.invoice_title_type = this.orderInvoice.invoice_title_type == undefined? 1:this.orderInvoice.invoice_title_type;
			this.orderInvoice.invoice_conten_index = this.orderInvoice.invoice_conten_index?this.orderInvoice.invoice_conten_index:0;
			this.orderInvoice.orderInvoice = this.orderInvoice.orderInvoice?this.orderInvoice.orderInvoice:0;
			this.orderInvoice.invoice_content = this.orderInvoice.invoice_content?this.orderInvoice.invoice_content:'';
			this.orderInvoice.invoice_title = this.orderInvoice.invoice_title?this.orderInvoice.invoice_title:'';
			this.orderInvoice.invoice_full_address = this.orderInvoice.invoice_full_address?this.orderInvoice.invoice_full_address:''; //邮寄地址
			this.orderInvoice.is_tax_invoice = this.orderInvoice.is_tax_invoice?this.orderInvoice.is_tax_invoice:0; //是否需要增值税发票
			this.orderInvoice.invoice_email = this.orderInvoice.invoice_email?this.orderInvoice.invoice_email:''; // 邮箱
			this.orderInvoice.taxpayer_number = this.orderInvoice.taxpayer_number?this.orderInvoice.taxpayer_number:''; //纳税人识别号
			this.openPopup('invoicePopup')
		},
		// 切换发票开关
		changeIsInvoice() {
			if (this.orderInvoice.is_invoice == 0) {
				this.orderInvoice.is_invoice = 1;
			} else {
				this.orderInvoice.is_invoice = 0;
			}
			this.$forceUpdate();
		},
		// 切换发票类型
		changeInvoiceType(invoice_type) {
			this.orderInvoice.invoice_type = invoice_type;
			this.$forceUpdate();
		},
		// 切换发票个人还是企业
		changeInvoiceTitleType(invoice_title_type) {
			this.orderInvoice.invoice_title_type = invoice_title_type;
			this.$forceUpdate();
		},
		// 选择发票内容
		changeInvoiceContent(index,invoice_content) {
			this.currInvoiceIndex = index
			this.orderInvoice.invoice_content = invoice_content;
			this.$forceUpdate();
		},
		//关闭发票弹窗
		closeInvoicePopup(){
			
			this.orderPaymentData.shop_goods_list[this.orderInvoice.site_id] = this.orderInvoice
			//发票
			
			if (this.orderInvoice.is_invoice == 1) {
				if(this.orderInvoice.invoice_title == ''){
					
					this.$util.showToast({title:"请填写发票抬头"})
					return false
				}
				if(this.orderInvoice.invoice_title_type == 2 && this.orderInvoice.taxpayer_number == ''){
					this.$util.showToast({title:"请填写纳税人识别号"})
					return false
				}
				if(this.orderInvoice.invoice_type == 1 && this.orderInvoice.invoice_full_address == ''){
					this.$util.showToast({title:"请填写邮寄地址"})
					return false
				}
				if(this.orderInvoice.invoice_type == 2 && this.orderInvoice.invoice_email == ''){
					this.$util.showToast({title:"请填写邮箱地址"})
					return false
				}
				if(this.orderInvoice.invoice_type == 2 && this.orderInvoice.invoice_email != ''){
					var reg = /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/;
					if (!reg.test(this.orderInvoice.invoice_email)) {
						this.$util.showToast({title:"请填写正确的邮箱地址"})
						return false
					}
				}
				if(this.orderInvoice.invoice_content == ''){
					this.$util.showToast({title:"请选择发票内容"})
					return false
				}
				
			}
			this.handlePaymentData()
			this.closePopup('invoicePopup')
			// this.network = false
			
		},
		// 显示弹出赠送的优惠券
		openCouponPopup(item){
			this.openPopup('presentCouponPopup')
			this.presentCoupon = item
		},
		/**
		 * 显示弹出层
		 * @param {Object} ref
		 */
		openPopup(ref) {
			if(ref=='PlatcouponPopup'){
				this.selectPlatCouponId=this.orderPaymentData.platform_coupon_id;
				this.selectPlatCouponMoney=this.orderPaymentData.platform_coupon_money; 
			}
			this.$refs[ref].open();
		},
		/**
		 * 关闭弹出层
		 * @param {Object} ref
		 */
		closePopup(ref) {
			this.network = false
			if (this.tempData) {
				Object.assign(this.orderCreateData, this.tempData);
				Object.assign(this.orderPaymentData, this.tempData);
				this.tempData = null;
				this.$forceUpdate();
			}
			this.$refs[ref].close();
		},
		/**
		 * 选择收货地址
		 */
		selectAddress() {
			this.$util.redirectTo('/otherpages/member/address/address', {
				'back': '/promotionpages/enterprisebuy/payment/payment'
			});
		},
		/**
		 * 获取订单初始化数据
		 */
		getOrderPaymentData() {
			this.orderCreateData = uni.getStorageSync('orderCreateData');
			if (!this.orderCreateData) {
				this.$util.showToast({
					title: '未获取到创建订单所需数据!！',
					success: () => {
						setTimeout(() => {
							this.$util.redirectTo('/pages/index/index/index', {}, 'reLaunch');
						}, 1500)
					}
				});
				return;
			}
			this.$api.sendRequest({
				url: '/corporatesubscription/api/ordercreate/payment',
				data: this.orderCreateData,
				success: res => {
					if (res.code >= 0) {
						for(var index in res.data.shop_goods_list){
							if(!res.data.shop_goods_list[index].present_list){
								res.data.shop_goods_list[index].present_list = {
									coupon_list:[]
								}
							}else if(res.data.shop_goods_list[index].present_list.goods_list){
								 res.data.shop_goods_list[index].goods_list = res.data.shop_goods_list[index].goods_list.concat(res.data.shop_goods_list[index].present_list.goods_list)
							}else if(!res.data.shop_goods_list[index].present_list.coupon_list){
								res.data.shop_goods_list[index].present_list.coupon_list = []
							}
						}
						this.orderPaymentData = res.data;
						
						this.handlePaymentData();
						if (this.$refs.loadingCover) this.$refs.loadingCover.hide();
					} else {
						this.$util.showToast({
							title: '未获取到创建订单所需数据!！',
							success: () => {
								setTimeout(() => {
									this.$util.redirectTo('/pages/index/index/index', {}, 'reLaunch');
								}, 1500)
							}
						});
					}
				},
				fail: res => {
					if (this.$refs.loadingCover) this.$refs.loadingCover.hide();
				}
			})
		},
		/**
		 * 处理结算订单数据
		 */
		handlePaymentData() {
			this.orderCreateData.delivery = {};
			this.orderCreateData.invoice = {};
			this.orderCreateData.coupon = {};
			this.orderCreateData.buyer_message = {};

			this.orderCreateData.is_balance = 0;
			this.orderCreateData.pay_password = '';

			var data = this.orderPaymentData;
			
			let h = new Date().getHours().toString();
			let m = new Date().getMinutes().toString();
			if (h.length == 1) {
				h = '0' + h;
			}
			if (m.length == 1) {
				m = '0' + m;
			}
			let nowTime = h + ':' + m;

			Object.keys(data.shop_goods_list).forEach((key, index) => {
				let siteItem = data.shop_goods_list[key];
				// 店铺配送方式
				this.orderCreateData.delivery[key] = {};
				if(siteItem.local_config){
					if (siteItem.local_config.info && siteItem.local_config.info.time_is_open == 1) {
						this.orderCreateData.delivery[key].showTimeBar=true;
						this.orderCreateData.delivery[key].buyer_ask_delivery_time=nowTime;
					}else{
						this.orderCreateData.delivery[key].showTimeBar=false;
					}
				}
				
				//店铺发票
				if(siteItem.is_invoice) {
					this.orderCreateData.invoice[key] = {};
				}
				
				if(siteItem.is_invoice){
					if (siteItem.is_invoice && !Array.isArray(siteItem.invoice_config)) {
						this.orderCreateData.invoice[key].is_invoice = siteItem.is_invoice
						this.orderCreateData.invoice[key].invoice_type = siteItem.invoice_type
						this.orderCreateData.invoice[key].invoice_title = siteItem.invoice_title
						this.orderCreateData.invoice[key].taxpayer_number = siteItem.taxpayer_number
						this.orderCreateData.invoice[key].invoice_content = siteItem.invoice_content
						this.orderCreateData.invoice[key].invoice_full_address = siteItem.invoice_full_address
						this.orderCreateData.invoice[key].is_tax_invoice = siteItem.is_tax_invoice
						this.orderCreateData.invoice[key].invoice_email = siteItem.invoice_email
						this.orderCreateData.invoice[key].invoice_title_type = siteItem.invoice_title_type
					}
					
				}
				
				if (siteItem.express_type[0] != undefined) {
					this.orderCreateData.delivery[key].delivery_type = siteItem.express_type[0].name;
					this.orderCreateData.delivery[key].delivery_type_name = siteItem.express_type[0].title;
					this.orderCreateData.delivery[key].store_id = 0;
					this.orderCreateData.delivery[key].store_id = 0;

					// 如果是门店配送
					if (siteItem.express_type[0].name == 'store') {
						if (siteItem.express_type[0].store_list[0] != undefined) {
							this.orderCreateData.delivery[key].store_id = siteItem.express_type[0].store_list[0].store_id;
						}
					}	
				}

				// 店铺优惠券
				this.orderCreateData.coupon[key] = {};
				if (siteItem.coupon_list[0] != undefined) {
					this.orderCreateData.coupon[key].coupon_id = siteItem.coupon_list[0].coupon_id;
					this.selectCouponId=siteItem.coupon_list[0].coupon_id;
					this.orderCreateData.coupon[key].coupon_money = siteItem.coupon_list[0].money;
					this.selectCouponMoney=siteItem.coupon_list[0].coupon_id;
				}

				this.orderCreateData.buyer_message[key] = '';
			})

			if (this.orderPaymentData.is_virtual) this.orderCreateData.member_address = {
				mobile: ''
			};
			
		
			
			
			if(this.orderPaymentData.platform_coupon_list.length>0){
				this.orderPaymentData.platform_coupon_id=this.orderPaymentData.platform_coupon_list[0].platformcoupon_id;
				this.orderCreateData.platform_coupon_id=this.orderPaymentData.platform_coupon_list[0].platformcoupon_id;
				this.orderPaymentData.platform_coupon_money=this.orderPaymentData.platform_coupon_list[0].money;
				this.orderCreateData.platform_coupon_money=this.orderPaymentData.platform_coupon_list[0].money;
				
				this.selectPlatCouponId=this.orderPaymentData.platform_coupon_list[0].platformcoupon_id;
				this.selectPlatCouponMoney=this.orderPaymentData.platform_coupon_list[0].money;
			}
		

			Object.assign(this.orderPaymentData, this.orderCreateData);
			this.orderCalculate();
		},
		/**
		 * 订单计算
		 */
		orderCalculate() {
			var data = this.$util.deepClone(this.orderCreateData);
			data.delivery = JSON.stringify(data.delivery);
			data.coupon = JSON.stringify(data.coupon);
			data.invoice = JSON.stringify(data.invoice);
			data.member_address = JSON.stringify(data.member_address);
			this.$api.sendRequest({
				url: '/corporatesubscription/api/ordercreate/calculate',
				data,
				success: res => {
					if (res.code >= 0) {
						this.orderPaymentData.delivery_money = res.data.delivery_money;
						this.orderPaymentData.coupon_money = res.data.coupon_money;
						this.orderPaymentData.invoice_money = res.data.invoice_money;
						this.orderPaymentData.invoice_delivery_money = res.data.invoice_delivery_money;
						
						this.orderPaymentData.promotion_money = res.data.promotion_money;
						this.orderPaymentData.order_money = res.data.order_money;
						this.orderPaymentData.balance_money = res.data.balance_money;
						this.orderPaymentData.pay_money = res.data.pay_money;
						this.orderPaymentData.goods_money = res.data.goods_money;

						Object.keys(res.data.shop_goods_list).forEach((key, index) => {
							let siteItem = res.data.shop_goods_list[key];
							
							this.orderPaymentData.shop_goods_list[key].order_money = siteItem.order_money;
							this.orderPaymentData.shop_goods_list[key].pay_money = siteItem.pay_money;
							this.orderPaymentData.shop_goods_list[key].coupon_money = siteItem.coupon_money;
							this.orderPaymentData.shop_goods_list[key].invoice_money = siteItem.invoice_money;
							this.orderPaymentData.shop_goods_list[key].invoice_delivery_money = siteItem.invoice_delivery_money;
						})
					} else {
						this.$util.showToast({
							title: res.message
						});
					}
				},
			})
		},
		/**
		 * 订单创建
		 */
		orderCreate() {
			if (this.verify()) {
				if (this.isSub) return;
				this.isSub = true;
				var data = this.$util.deepClone(this.orderCreateData);

				data.delivery = JSON.stringify(data.delivery);
				data.coupon = JSON.stringify(data.coupon);
				data.invoice = JSON.stringify(data.invoice);
				data.member_address = JSON.stringify(data.member_address);
				data.buyer_message = JSON.stringify(data.buyer_message);

				this.$api.sendRequest({
					url: '/corporatesubscription/api/ordercreate/create',
					data,
					success: res => {
						uni.hideLoading();
						if (res.code >= 0) {
							uni.removeStorage({
								key: 'orderCreateData',
								success: () => {
									if (this.orderPaymentData.pay_money == 0) {
										this.$util.redirectTo('/pages/pay/result/result', {
											code: res.data
										}, 'redirectTo');
									} else {
										this.$util.redirectTo('/pages/pay/index/index', {
											code: res.data
										}, 'redirectTo');
									}
								}
							});
						} else {
							this.isSub = false;
							uni.hideLoading();
							if(this.$refs.payPassword){
								this.$refs.payPassword.close();
							}
							if (res.data.error_code == 10 || res.data.error_code == 12) {
								uni.showModal({
									title: '订单未创建',
									content: res.message,
									confirmText: '去设置',
									success: res => {
										if (res.confirm) {
											this.selectAddress();
										}
									}
								})
							} else {
								this.$util.showToast({
									title: res.message
								});
							}
						}
						this.getCartNumber();
					},
					fail: res => {
						uni.hideLoading();
						this.isSub = false;
					}
				})
			}
		},
		/**
		 * 订单验证
		 */
		verify() {
			if (this.orderPaymentData.is_virtual == 1) {
				if (!this.orderCreateData.member_address.mobile.length) {
					this.$util.showToast({
						title: '请输入您的手机号码'
					});
					return false;
				}
				var reg = /^[1](([3][0-9])|([4][5-9])|([5][0-3,5-9])|([6][5,6])|([7][0-8])|([8][0-9])|([9][1,8,9]))[0-9]{8}$/;
				if (!reg.test(this.orderCreateData.member_address.mobile)) {
					this.$util.showToast({
						title: '请输入正确的手机号码'
					});
					return false;
				}
			}

			if(this.orderPaymentData.is_need_idcard == 1) {
				if(this.orderCreateData.id_card == undefined || this.orderCreateData.id_card == '') {
					this.$util.showToast({
						title: '不能为空，请输入您的身份证号'
					})
					return false;
				}
				
				// "^[1-9](\\d{5})(19|20)(\\d{2})((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)(\\d{3})(\\d|X|x)$"
				// var re15 = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/;
				// var re18 = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{4}$/;
				// var res = (re15.test(this.orderCreateData.id_card) || re18.test(this.orderCreateData.id_card));
				
				var iscardNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
				var res = iscardNo.test(this.orderCreateData.id_card)
				if(res == false) {
					this.$util.showToast({
						title: '请输入您正确的身份证号码'
					});
					return false;
				}
			}
			
			
			if(this.orderPaymentData.is_need_idcard == 1) {
				console.log(this.orderCreateData.real_name)
				if(this.orderCreateData.real_name == undefined || this.orderCreateData.real_name == '') {
					this.$util.showToast({
						title: '不能为空，请输入您的真实姓名'
					});
					return false;
				}
				
				//验证中文名字
				var re = /^[\u4e00-\u9fa5]+$/;
				var ress = re.test(this.orderCreateData.real_name);
				if(!ress) {
					this.$util.showToast({
						title: '只能输入中文姓名'
					});
					return false;
				}
			}
			
			if (this.orderPaymentData.is_virtual == 0 && this.orderPaymentData.is_need_idcard == 0) {
				if (!this.orderPaymentData.member_address) {
					this.$util.showToast({
						title: '请先选择您的收货地址'
					});
					return false;
				}

				let deliveryVerify = true;

				for (let key in this.orderCreateData.delivery) {
					if (JSON.stringify(this.orderCreateData.delivery[key]) == "{}") {
						deliveryVerify = false;
						this.$util.showToast({
							title: '店铺"' + this.orderPaymentData.shop_goods_list[key].site_name + '"未设置配送方式'
						});
						break;
					}
					if (this.orderCreateData.delivery[key].delivery_type == 'store' && this.orderCreateData.delivery[key].store_id ==
						0) {
						deliveryVerify = false;
						this.$util.showToast({
							title: '店铺"' + this.orderPaymentData.shop_goods_list[key].site_name + '"没有可提货的门店,请选择其他配送方式'
						});
						break;
					}
				}
				if (!deliveryVerify) return false;
			}
			if (this.orderCreateData.is_balance == 1 && this.orderCreateData.pay_password == '') {
				setTimeout(() => {
					this.$refs.input.clear();
				}, 0)
				// this.$refs.payPassword.open();
				this.openPasswordPopup();
				return false;
			}
			return true;
		},
		/**
		 * 显示店铺优惠信息
		 * @param {Object} data
		 */
		openSitePromotion(data) {
			this.sitePromotion = data;
			this.$refs.sitePromotionPopup.open();
		},
		/**
		 * 显示店铺配送信息
		 * @param {Object} index
		 */
		openSiteDelivery(siteId, deliveryData) {
			this.tempData = {
				delivery: this.$util.deepClone(this.orderPaymentData.delivery)
			};

			this.siteDelivery.site_id = siteId;
			this.siteDelivery.data = deliveryData;
			this.$refs.deliveryPopup.open();
		},
		/**
		 * 选择配送方式
		 */
		selectDeliveryType(data) {
			this.orderCreateData.delivery[this.siteDelivery.site_id].delivery_type = data.name;
			this.orderCreateData.delivery[this.siteDelivery.site_id].delivery_type_name = data.title;
			// 如果是门店配送
			if (data.name == 'store') {
				if (data.store_list[0] != undefined) {
					this.orderCreateData.delivery[this.siteDelivery.site_id].store_id = data.store_list[0].store_id;
				}
			}
			Object.assign(this.orderPaymentData, this.orderCreateData);
			this.$forceUpdate();
		},
		/**
		 * 选择自提点 
		 */
		selectPickupPoint(store_id) {
			this.orderCreateData.delivery[this.siteDelivery.site_id].store_id = store_id;
			Object.assign(this.orderPaymentData, this.orderCreateData);
			this.$forceUpdate();
		},
		/**
		 * 显示店铺优惠券信息
		 * @param {Object} siteId
		 * @param {Object} couponData
		 */
		openSiteCoupon(siteId, couponData,siteIndex) {
			this.tempData = {
				coupon: this.$util.deepClone(this.orderPaymentData.coupon)
			};
			
			this.siteCoupon.site_id = siteId;
			this.selectCouponId=this.orderCreateData.coupon[siteId].coupon_id;
			this.selectCouponMoney=this.orderCreateData.coupon[siteId].coupon_money;
			
			this.siteCoupon.data = couponData;
			this.$refs.couponPopup.open();
		},
		/**
		 * 选择优惠券
		 * @param {Object} item
		 */
		selectCoupon(item) {
			if (this.selectCouponId != item.coupon_id) {
				this.selectCouponId = item.coupon_id;
				// this.orderCreateData.coupon[this.siteCoupon.site_id].coupon_money = this.orderPaymentData.shop_goods_list[item.site_id].coupon_money;
			} else {
				this.selectCouponId = 0;
				// this.orderCreateData.coupon[this.siteCoupon.site_id].coupon_id = 0;
				// this.orderCreateData.coupon[this.siteCoupon.site_id].coupon_money = '0.00';
			}
			Object.assign(this.orderPaymentData, this.orderCreateData);
			this.$forceUpdate();
		},
		/**
		 * 选择平台优惠券
		 * @param {Object} item
		 */
		selectPlatCoupon(item){
			
			if(this.selectPlatCouponId != item.platformcoupon_id){
				this.selectPlatCouponId=item.platformcoupon_id;
				this.selectPlatCouponMoney=item.money;
				// this.orderPaymentData.platform_coupon_id=item.platformcoupon_id;
				// this.orderCreateData.platform_coupon_id=item.platformcoupon_id;
				// this.orderPaymentData.platform_coupon_money=item.money;
				// this.orderCreateData.platform_coupon_money=item.money;
			}else{
				this.selectPlatCouponId=0;
				this.selectPlatCouponMoney='0.00';
			}
			Object.assign(this.orderPaymentData, this.orderCreateData);
			this.$forceUpdate();
		},
		popupConfirm(ref,site_id) {
			this.$refs[ref].close();
			if(ref=='couponPopup'){
				this.orderCreateData.coupon[site_id].coupon_id = this.selectCouponId;
				this.orderCreateData.coupon[site_id].coupon_money = this.selectCouponMoney;
			}
			if(ref=='PlatcouponPopup'){
				this.orderPaymentData.platform_coupon_id=this.selectPlatCouponId;
				this.orderCreateData.platform_coupon_id=this.selectPlatCouponId;
				this.orderPaymentData.platform_coupon_money=this.selectPlatCouponMoney;
				this.orderCreateData.platform_coupon_money=this.selectPlatCouponMoney;
			}
			this.orderCalculate();
		},
		/**
		 * 使用余额
		 */
		useBalance() {
			if (this.orderCreateData.is_balance) this.orderCreateData.is_balance = 0;
			else this.orderCreateData.is_balance = 1;
			this.orderCalculate();
			this.$forceUpdate();
		},
		/**
		 * 设置支付密码
		 */
		setPayPassword() {
			this.$util.redirectTo('/otherpages/member/pay_password/pay_password', {
				back: '/promotionpages/order/payment/payment'
			});
		},
		/**
		 * 暂不设置支付密码
		 */
		noSet() {
			this.orderCreateData.is_balance = 0;
			this.$refs.payPassword.close();
			this.orderCalculate();
			this.$forceUpdate();
		},
		/**
		 * 支付密码输入
		 */
		input(pay_password) {
			if (pay_password.length == 6) {
				uni.showLoading({
					title: '支付中...',
					mask: true
				})
				this.$api.sendRequest({
					url: '/api/member/checkpaypassword',
					data: {
						pay_password
					},
					success: res => {
						if (res.code >= 0) {
							this.orderCreateData.pay_password = pay_password;
							this.orderCreate();
						} else {
							uni.hideLoading();
							this.$util.showToast({
								title: res.message
							});
						}
					},
					fail: res => {
						uni.hideLoading();
					}
				})
			}
		},
		imageError(siteIndex, goodsIndex) {
			this.orderPaymentData.shop_goods_list[siteIndex].goods_list[goodsIndex].sku_image = this.$util.getDefaultImage().default_goods_img;
			this.$forceUpdate();
		},
		// 购物车数量
		getCartNumber() {
			if (uni.getStorageSync("token")) {
				this.$store.dispatch('getCartNumber')
			}
		},
		openPasswordPopup(){
			this.$refs.payPassword.open();
			setTimeout(()=>{
				this.isFocus = true;
			},500)
		},
	},
	onShow() {
		// 刷新多语言
		this.$langConfig.refresh();

		// 判断登录
		if (!uni.getStorageSync('token')) {
			this.$util.redirectTo('/pages/login/login/login');
		} else {
			this.orderPaymentData= {
				member_account: {
					balance: 0,
					is_pay_password: 0
				}
			}
			this.orderCreateData= {
				is_balance: 0,
				pay_password: '',
				platform_coupon_id:0,
				buyer_message:{}
			}
			this.getOrderPaymentData();
		}
	},
	onHide() {
		if (this.$refs.loadingCover) this.$refs.loadingCover.show();
	},
	filters: {
		/**
		 * 金额格式化输出
		 * @param {Object} money
		 */
		moneyFormat(money) {
			return parseFloat(money).toFixed(2);
		},
		/**
		 * 店铺优惠摘取
		 */
		promotion(data) {
			let promotion = '';
			if (data) {
				Object.keys(data).forEach((key) => {
					promotion += data[key].content + '　';
				})
			}
			return promotion;
		}
	}
}
